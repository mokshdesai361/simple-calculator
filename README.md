# Simple Calculator

Simple Calculator Made In C Language

## Getting started

To use this program follow the steps that are mentioned below:


1. Make sure that gcc basic c compiler is installed in order to compile the program


2. Then clone the repository,


3. Then do ``cd simple-calculator`` In terminal to change the directory of console,


4. After changing into project's directory open a terminal in the directory,


5. Then write ``gcc -o main.o main.c`` To compile the program,


6. After compiling the program just write this in your terminal ``./main.o``.

Enjoy, Do Your Mathematical Operations.
